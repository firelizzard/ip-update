package files

import (
	"embed"
	"text/template"
)

//go:embed *.service *.timer
var clientFiles embed.FS

func LoadClientTemplates() (*template.Template, error) {
	return template.ParseFS(clientFiles, "*.service", "*.timer")
}
