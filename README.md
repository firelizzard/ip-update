# IP Update

The IP Update project is a simple set of tools that can be used to keep track of
the IP addresses of systems with dynamically allocated network addresses.

The depot accepts notifications and stores a database of last-known IP
addresses. JWT notifications are constructed, signed, and sent by the client
tool.

## Install

This project does not provide prebuilt binaries, so you must have Go 1.16 or
later to compile. The tools can be compiled with `go build` or with
`./build.sh`. The latter embeds the git revision. These instalation instructions
assume that 1) the server is the authoritative nameserver for the DNS zone in
question, 2) unauthenticated (ns)updates are allowed from localhost, and 3)
apache/nginx/etc is acting as a reverse proxy for the HTTP server. The IP update
endpoint (`PUT /`) *should* be secure against MITM attacks, but the IP retrieval
endpoint (`GET /ip`) *certainly* should not be used if served via plain HTTP.
The depot can serve HTTPS directly, with `--listen https://...` and
`--tls-cert`/`--tls-key`.

```shell
# Build server
./build.sh ./cmd/ip-update-depot

# Install binary and script
sudo cp ip-update-depot files/on-ip-update /usr/local/bin/

# Install service
ip-update-depot --listen http://localhost:12345 --script /usr/local/bin/on-ip-update install

# Start service on boot, and start now
systemctl enable --now ip-update-depot
```

```shell
# Build client
./build.sh ./cmd/send-ip-update

# Install binary
sudo cp ip-update-depot /usr/local/bin/

# Create user and key
sudo mkdir /var/ip-update
sudo useradd -r -d /var/ip-update -s /sbin/nologin ip-update
sudo -u ip-update mkdir /var/ip-update/.ssh
sudo -u ip-update ssh-keygen -t ecdsa -b 521 -C my-home-ip.dyn.example.com -f /var/ip-update/.ssh/id_ecdsa -N ''
sudo cat /var/ip-update/.ssh/id_ecdsa.pub # copy this to /etc/ip-update/accept on the host

# Install service and timer
sudo send-ip-update install --timer --service --ip-from https://depot.example.com/ip https://depot.example.com

# Enable timer
systemctl enable --now send-ip-update.timer
```

## `send-ip-update`

The client tool constructs a JWT, including the fingerprint of the signing key
as the JWT issuer. The token can include an IPv4 and/or IPv6 address, or it can
specify that the address(es) should be inferred from the TCP connection.

## Depot {`ip-update-depot`}

The depot must be accessible to clients via HTTP(S). When the depot receives a
JWT, it compares the fingerprint (JWT issuer) to the list of accepted keys in
`/etc/ip-update/accept`. If an entry with the same fingerprint is found, the
corresponding entry is created or updated.

### Entries

Depot entries include a name, IPv4, IPv6, and timestamp of the last update.
Entries are persisted to `/var/db/ip-update/depot`. When a JWT is received, if
there is an entry in `/etc/ip-update/accept` with a key with a matching
fingerprint, the name/comment of that entry is used for the depot entry. For
example:

```
ecdsa-sha2-nistp256 AAAAE2VjZHNh... my-pc.example.com
```

Would result in a depot entry named `my-pc.example.com`. There are no
requirements on what the name/comment looks like, as long as it is present (and
not blank).

### Update Script

If `--script <script>` specified, `<script>` is called whenever the depot is
written to disk. The depot entries are passed to the script via stdin as
comma-separated values, one per line. For example:

```
my-pc-1.example.com,1.2.3.4,<ipv6>,<timestamp>
my-pc-2.example.com,5.6.7.8,<ipv6>,<timestamp>
```

## Flow

1. {Client} Capture WAN IP address.
1. {Client} Construct signed JWT, including public key fingerprint.
1. Transmit.
1. {Server} Verify that the JWT is not expired (or too young).
1. {Server} Search `/etc/ip-update/accept` for an entry with a key with a matching fingerprint.
1. {Server} Validate the JWT signature using the matching public key.
1. {Server} Using the name/comment of the matching entry, update the depot.
1. {Server} Some time later, write `/var/db/ip-update/depot` and execute the update script.