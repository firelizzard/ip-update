module gitlab.com/firelizzard/ip-update

go 1.16

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/kardianos/service v1.2.0
	github.com/spf13/cobra v1.1.3
	github.com/spf13/pflag v1.0.5
	golang.org/x/crypto v0.0.0-20210513164829-c07d793c2f9a
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
	golang.org/x/sys v0.0.0-20201119102817-f84b799fce68
	golang.org/x/term v0.0.0-20210503060354-a79de5458b56
)
