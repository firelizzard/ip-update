#!/bin/bash

go build -ldflags "-X main.Version=$(git rev-list -1 HEAD)$([[ -z $(git status -s) ]] || echo '-dirty')" "$@"