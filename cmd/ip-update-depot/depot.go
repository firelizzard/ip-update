package main

import (
	"encoding/json"
	"errors"
	"io/fs"
	"os"
	"sync"
	"time"
)

var Depot _Depot

type _Depot struct {
	mu sync.RWMutex
	DepotData
}

type DepotData struct {
	Domains []*DomainData
}

type DomainData struct {
	Name      string
	IPv4      string
	IPv6      string
	UpdatedOn time.Time
}

func (d *_Depot) Load() error {
	f, err := os.Open("/var/db/ip-update/depot")
	if errors.Is(err, fs.ErrNotExist) {
		return nil
	} else if err != nil {
		return err
	}
	defer f.Close()

	var c DepotData
	err = json.NewDecoder(f).Decode(&c)
	if err != nil {
		return err
	}

	d.mu.Lock()
	d.DepotData = c
	d.mu.Unlock()
	return nil
}

func (d *DepotData) Save() error {
	f, err := os.OpenFile("/var/db/ip-update/depot", os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0600)
	if err != nil {
		return err
	}
	defer f.Close()

	return json.NewEncoder(f).Encode(d)
}

func (d *_Depot) Get() DepotData {
	d.mu.Lock()
	c := d.DepotData
	d.mu.Unlock()
	return c
}

func (d *_Depot) Update(do func(func(domain, ipv4, ipv6 string))) {
	d.mu.Lock()
	defer d.mu.Unlock()

	do(func(domain, ipv4, ipv6 string) {
		var t time.Time
		if ipv4 != "" || ipv6 != "" {
			t = time.Now()
		}

		for _, entry := range d.Domains {
			if entry.Name == domain {
				if ipv4 != "" {
					entry.IPv4 = ipv4
				}
				if ipv6 != "" {
					entry.IPv6 = ipv6
				}
				entry.UpdatedOn = t
				return
			}
		}

		d.Domains = append(d.Domains, &DomainData{
			Name:      domain,
			IPv4:      ipv4,
			IPv6:      ipv6,
			UpdatedOn: t,
		})
	})
}
