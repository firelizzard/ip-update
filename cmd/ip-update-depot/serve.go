package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/kardianos/service"
)

type Serve struct {
	log service.Logger

	GET, PUT bool
	Updates  chan DomainData
}

func (s *Serve) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	defer func() {
		r := recover()
		if r != nil {
			s.log.Errorf("Panic! %v", r)
		}
	}()

	if r.Method == http.MethodGet && r.URL.Path == "/ip" {
		s.GetIP(w, r)
		return
	}

	if s.GET && r.Method == http.MethodGet && r.URL.Path == "/" {
		s.Get(w, r)
		return
	}

	if s.PUT && r.Method == http.MethodPut && r.URL.Path == "/" {
		s.Put(w, r)
		return
	}

	if r.Method == http.MethodGet {
		w.WriteHeader(http.StatusNotFound)
	} else {
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func (s *Serve) GetIP(w http.ResponseWriter, r *http.Request) {
	addr := r.Context().Value(http.LocalAddrContextKey)
	var ip net.IP
	switch addr := addr.(type) {
	case net.IP:
		ip = addr
	case *net.TCPAddr:
		ip = addr.IP
	}

	if fwd, ok := r.Header["X-Forwarded-For"]; ok && len(fwd) > 0 {
		parsed := net.ParseIP(fwd[0])
		if parsed == nil {
			s.log.Infof("Invalid X-Forwarded-For: %v", fwd)
		} else {
			ip = parsed
		}
	}

	if p4 := ip.To4(); len(p4) == net.IPv4len {
		w.Write([]byte(p4.String() + "\n"))
	} else if len(ip) == net.IPv6len {
		w.Write([]byte(ip.String() + "\n"))
	} else {
		s.log.Infof("Invalid IP: %v", ip)
	}
}

func (s *Serve) Get(w http.ResponseWriter, r *http.Request) {
	b, err := json.Marshal(Depot.Get())
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		s.log.Warning(err)
	} else {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(b)
	}
}

func (s *Serve) Put(w http.ResponseWriter, r *http.Request) {
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		s.log.Warning(err)
		return
	}

	accept := Config.Get().Accept
	nope := errors.New("")

	var accepted *Accept
	var claims jwt.StandardClaims
	tok, err := jwt.ParseWithClaims(string(b), &claims, func(t *jwt.Token) (interface{}, error) {
		if claims.IssuedAt < time.Now().Add(-5*time.Minute).Unix() {
			w.WriteHeader(http.StatusBadRequest)
			return nil, nope
		}

		if claims.IssuedAt > time.Now().Add(+5*time.Minute).Unix() {
			w.WriteHeader(http.StatusBadRequest)
			return nil, nope
		}

		for _, accepted = range accept {
			if accepted.Fingerprint == claims.Issuer {
				return accepted.Key.CryptoPublicKey(), nil
			}
		}

		return nil, nope
	})
	if err != nil {
		var verr *jwt.ValidationError
		isVErr := errors.As(err, &verr)

		if err == nope || (isVErr && verr.Inner == nope) {
			// nothing to print
		} else if isVErr {
			s.log.Warningf("invalid JWT - %v", err)
		} else {
			s.log.Warning(err)
		}
		return
	}

	ipv4 := fmt.Sprint(tok.Header["ipv4"])
	ipv6 := fmt.Sprint(tok.Header["ipv6"])

	s.Updates <- DomainData{
		Name:      accepted.Domain,
		IPv4:      ipv4,
		IPv6:      ipv6,
		UpdatedOn: time.Now(),
	}

	Depot.Update(func(f func(domain, ipv4, ipv6 string)) {
		f(accepted.Domain, ipv4, ipv6)
	})
}
