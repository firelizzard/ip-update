package main

import (
	"bufio"
	"errors"
	"io"
	"log"
	"os"
	"regexp"
	"strings"
	"sync"

	"golang.org/x/crypto/ssh"
)

var reAcceptComment = regexp.MustCompile(`#.*$`)
var reSpace = regexp.MustCompile(`\s`)

var Config _Config

type _Config struct {
	mu sync.Mutex
	ConfigData
}

type ConfigData struct {
	Accept []*Accept
}

type Accept struct {
	Domain      string
	Key         SSHCryptoPublicKey
	Fingerprint string
	Comment     string
}

type SSHCryptoPublicKey interface {
	ssh.PublicKey
	ssh.CryptoPublicKey
}

func (c *_Config) Load() error {
	var d ConfigData

	if err := d.LoadAccept(); err != nil {
		return err
	}

	c.mu.Lock()
	c.ConfigData = d
	c.mu.Unlock()
	return nil
}

func (c *ConfigData) LoadAccept() error {
	f, err := os.Open(*acceptFile)
	if err != nil {
		return err
	}
	defer f.Close()

	r := bufio.NewReader(f)
	for {
		l, err := r.ReadString('\n')
		if errors.Is(err, io.EOF) {
			break
		} else if err != nil {
			return err
		}

		l = reAcceptComment.ReplaceAllString(l, "")
		l = strings.TrimSpace(l)
		if len(l) == 0 {
			continue
		}

		key, comment, _, rest, err := ssh.ParseAuthorizedKey([]byte(l))
		if err != nil {
			log.Printf("%s: invalid key: %v", f.Name(), err)
			continue
		}

		if len(comment) == 0 {
			log.Printf("%s: skipping entry with missing comment: %q", f.Name(), l)
		}

		a := &Accept{
			Domain:      comment,
			Comment:     string(rest),
			Fingerprint: ssh.FingerprintSHA256(key),
		}

		if key, ok := key.(SSHCryptoPublicKey); ok {
			a.Key = key
		} else {
			log.Printf("%s: unsupported key type: %s", f.Name(), key.Type())
		}

		c.Accept = append(c.Accept, a)
	}

	return nil
}

func (c *_Config) Get() ConfigData {
	c.mu.Lock()
	d := c.ConfigData
	c.mu.Unlock()
	return d
}
