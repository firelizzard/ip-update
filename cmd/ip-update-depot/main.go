package main

import (
	"bufio"
	"context"
	"crypto/tls"
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"net"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"os/signal"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"github.com/kardianos/service"
	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
	"gitlab.com/firelizzard/ip-update/flags"
	"golang.org/x/sync/errgroup"
	"golang.org/x/sys/unix"
)

const (
	ServiceInstall   = "install"
	ServiceReinstall = "reinstall"
	ServiceUninstall = "uninstall"
	ServiceStart     = "start"
	ServiceStop      = "stop"
)

var Version = "version unknown"

func main() { app.Execute() }

var app = cobra.Command{
	Use:       "ip-update-depot [[un|re]install|start|stop]",
	Short:     "Depot server for IP updates",
	ValidArgs: []string{ServiceInstall, ServiceReinstall, ServiceUninstall, ServiceStart, ServiceStop},
}
var serviceConfig = &service.Config{
	Name:        "ip-update-depot",
	DisplayName: "IP Update Depot",
	Description: app.Short,
	Option: service.KeyValue{
		"ReloadSignal": "HUP",
		"LogOutput":    true,
	},
}

var listenAddr = flags.UrlArrayP(app.Flags(), "listen", "l", nil, "Address(s) to listen to for updates")
var adminListenAddr = flags.UrlArrayP(app.Flags(), "admin-listen", "a", nil, "Address(s) to listen to for administration")

var tickTime = app.Flags().Duration("tick", 10*time.Minute, "Set how often updates are written")
var updateScript = app.Flags().StringP("script", "s", "", "A script to run when the depot is updated")
var tlsCertFile = app.Flags().String("tls-cert", "", "TLS certificate")
var tlsKeyFile = app.Flags().String("tls-key", "", "TLS private key")

var acceptFile = app.Flags().String("accept", "/etc/ip-update/accept", "Override accepted keys file path")
var depotFile = app.Flags().String("depot", "/var/db/ip-update/depot", "Override depot persistence file path")

var createDepotDir = app.Flags().Bool("depot-mkdir", true, "If --depot specifies a path that does not exist, mkdir it")

var version = app.Flags().Bool("version", false, "Show version information")

var tlsCert *tls.Certificate

func init() {
	app.Flags().StringVar(&serviceConfig.UserName, "service-user", "", "Service user name")
	app.Flags().SortFlags = false

	app.Args = func(cmd *cobra.Command, args []string) error {
		if err := cobra.MaximumNArgs(1)(cmd, args); err != nil {
			return err
		}
		return cobra.OnlyValidArgs(cmd, args)
	}

	app.RunE = run

	app.MarkFlagFilename("accept")
	app.MarkFlagFilename("depot")
	app.MarkFlagFilename("tls-cert")
	app.MarkFlagFilename("tls-key")
	app.MarkFlagFilename("script")

	app.Flags().MarkHidden("depot-mkdir")
}

func run(cmd *cobra.Command, args []string) error {
	if *version {
		fmt.Printf("%s %s\n", app.Name(), Version)
		return nil
	}

	serviceConfig.Arguments = flags.Dump(cmd.Flags(), func(f *pflag.Flag) bool {
		return f.Name != "service-user"
	})

	ctx, cancel := context.WithCancel(context.Background())
	p := &Program{context: ctx, cancel: cancel, done: make(chan struct{})}
	serviceConfig.Option["RunWait"] = func() { <-p.done }
	s, err := service.New(p, serviceConfig)
	if err != nil {
		return err
	}

	l, err := s.Logger(nil)
	if err != nil {
		return err
	}

	if len(args) < 1 {
		err = s.Run()
		if err != nil {
			l.Error(err)
		}
		return nil
	}

	switch args[0] {
	case ServiceInstall, ServiceReinstall:
		err = p.preRun(l)
		if err != nil {
			return err
		}
	}

	switch args[0] {
	case ServiceInstall:
		return s.Install()
	case ServiceReinstall:
		err = s.Uninstall()
		if err != nil {
			return err
		}
		return s.Install()
	case ServiceUninstall:
		return s.Uninstall()
	case ServiceStart:
		return s.Start()
	case ServiceStop:
		return s.Stop()
	default:
		cmd.Usage()
		os.Exit(1)
		return nil
	}
}

type Program struct {
	mu   sync.Mutex
	done chan struct{}

	context context.Context
	cancel  context.CancelFunc

	runContext context.Context

	stopContext context.Context
	stopCancel  context.CancelFunc
}

func (p *Program) stop() {
	p.mu.Lock()
	defer p.mu.Unlock()
	defer p.cancel()

	if p.stopContext != nil {
		return
	}

	p.stopContext, p.stopCancel = context.WithTimeout(p.runContext, 5*time.Second)
}

func (p *Program) Start(s service.Service) error {
	log, err := s.Logger(nil)
	if err != nil {
		return err
	}

	if err := p.preRun(log); err != nil {
		return err
	}

	go func() {
		defer close(p.done)
		err := p.run(log)
		if err != nil {
			log.Error(err)
		}
	}()

	return nil
}

func (p *Program) Stop(service.Service) error {
	p.stop()
	<-p.stopContext.Done()
	return nil
}

func (p *Program) preRun(log service.Logger) error {
	var needTLS bool
	var hasCert = *tlsCertFile != ""
	var hasKey = *tlsKeyFile != ""
	var ports = map[string]string{}

	if *acceptFile == "" {
		return errors.New("--accept cannot be blank")
	}

	if *depotFile == "" {
		return errors.New("--depot cannot be blank")
	}

	cleanup := func(u *url.URL) error {
		var port string
		switch u.Scheme {
		case "":
			u.Scheme = "http"
			fallthrough
		case "http":
			port = "80"
		case "https":
			needTLS = true
			port = "443"
		default:
			return fmt.Errorf("%q is not a supported scheme", u.Scheme)
		}

		if u.Scheme == "" {
			u.Scheme = "http"
		}

		if u.Port() == "" {
			if strings.HasSuffix(u.Host, ":") {
				u.Host += port
			} else {
				u.Host += ":" + port
			}
		}

		if scheme, ok := ports[u.Host]; !ok {
			ports[u.Host] = u.Scheme
		} else if scheme != u.Scheme {
			return fmt.Errorf("cannot use both %q and %q at the same time on %q", scheme, u.Scheme, u.Host)
		}

		return nil
	}

	for _, addr := range *listenAddr {
		if err := cleanup(addr); err != nil {
			return err
		}
	}

	for _, addr := range *adminListenAddr {
		if err := cleanup(addr); err != nil {
			return err
		}
	}

	if needTLS {
		if !hasCert && !hasKey {
			return errors.New("--tls-cert and --tls-key are required for serving HTTPS")
		} else if !hasCert {
			return errors.New("--tls-cert is required for serving HTTPS")
		} else if !hasKey {
			return errors.New("--tls-key is required for serving HTTPS")
		}

		cert, err := tls.LoadX509KeyPair(*tlsCertFile, *tlsKeyFile)
		if err != nil {
			return fmt.Errorf("error loading --tls-cert/--tls-key: %w", err)
		}
		tlsCert = &cert
	} else if hasCert || hasKey {
		fmt.Fprintln(os.Stderr, "--tls-cert and --tls-key are ignored when not serving HTTPS")
	}

	return nil
}

func (p *Program) run(log service.Logger) error {
	runContext, runCancel := context.WithCancel(context.Background())
	defer runCancel()
	p.runContext = runContext

	var addrs = ListenSet{}
	var updates = make(chan DomainData, 10)

	if *createDepotDir {
		depotDir := filepath.Dir(*depotFile)

		if _, err := os.Stat(depotDir); errors.Is(err, fs.ErrNotExist) {
			err = os.Mkdir(depotDir, 0600)
			if err != nil {
				return err
			}
		}
	}

	if err := Config.Load(); err != nil {
		return err
	}

	if err := Depot.Load(); err != nil {
		return err
	}

	// ensure every accepted domain has at least a blank entry in the depot
	Depot.Update(func(fn func(domain, ipv4, ipv6 string)) {
		for _, a := range Config.Get().Accept {
			fn(a.Domain, "", "")
		}
	})

	for _, addr := range *listenAddr {
		addrs.Add(addr).PUT = true
	}

	for _, addr := range *adminListenAddr {
		addrs.Add(addr).GET = true
	}

	if len(addrs) == 0 {
		return errors.New("Specify at least one address to listen on")
	}

	sig := make(chan os.Signal)
	signal.Notify(sig, os.Interrupt, unix.SIGTERM, unix.SIGHUP)

	sigInfo := make(chan os.Signal)
	signal.Notify(sigInfo, unix.SIGUSR1, unix.SIGUSR2)

	go func() {
		for s := range sig {
			log.Infof("Received %v", s)

			switch s {
			case os.Interrupt, unix.SIGTERM:
				signal.Reset()
				log.Info("Shutting down\n")
				p.stop()

			case unix.SIGHUP:
				log.Info("Reloading configuration\n")
				if err := Config.Load(); err != nil {
					log.Error(err)
				}
			}
		}
	}()

	errg, ctx := errgroup.WithContext(p.context)

	errg.Go(func() error {
		var changed bool
		defer func() {
			if changed {
				p.updateDepot(log)
			}
		}()

		tick := time.Tick(*tickTime)
		for {
			select {
			case <-ctx.Done():
				return nil

			case <-tick:
				if changed {
					changed = false
					p.updateDepot(log)
				}

			case s := <-sigInfo:
				log.Infof("Received %v", s)
				if changed {
					changed = false
					p.updateDepot(log)
				} else {
					log.Warning("No updates")
				}

			case u, ok := <-updates:
				if !ok {
					return nil
				}

				changed = true
				msg := u.Name
				if u.IPv4 != "" {
					msg += " IPv4=" + u.IPv4
				}
				if u.IPv6 != "" {
					msg += " IPv6=" + u.IPv6
				}
				log.Info(msg)
			}
		}
	})

	for a, m := range addrs {
		a := a

		s := &http.Server{
			Handler: &Serve{
				log:     log,
				GET:     m.GET,
				PUT:     m.PUT,
				Updates: updates,
			},
			BaseContext: func(net.Listener) context.Context {
				return ctx
			},
		}

		var lc net.ListenConfig
		l, err := lc.Listen(ctx, "tcp", a.String())
		if err != nil {
			return err
		}

		go func() {
			<-ctx.Done()
			p.stop()
			log.Infof("Shutting down %v\n", a)
			s.Shutdown(p.stopContext)
		}()

		if m.Secure {
			l = tls.NewListener(l, &tls.Config{
				Certificates: []tls.Certificate{*tlsCert},
			})
		}

		errg.Go(func() error {
			err := s.Serve(l)
			if err != nil {
				log.Infof("%v: %v\n", err, a)
			}
			if !errors.Is(err, http.ErrServerClosed) {
				return err
			}
			return nil
		})
	}

	return errg.Wait()
}

type ListenAddr struct {
	Host string
	Port string
}

func (a ListenAddr) String() string { return fmt.Sprintf("%s:%s", a.Host, a.Port) }

type ListenMode struct {
	Secure, PUT, GET bool
}

type ListenSet map[ListenAddr]*ListenMode

func (s ListenSet) Add(u *url.URL) *ListenMode {
	l := ListenAddr{Host: u.Hostname(), Port: u.Port()}

	var secure bool
	switch u.Scheme {
	case "https":
		secure = true
	}

	m := &ListenMode{Secure: secure}
	s[l] = m
	return m
}

func (p *Program) updateDepot(log service.Logger) {
	log.Info("Saving depot")
	data := Depot.Get()
	err := data.Save()
	if err != nil {
		log.Error(err)
		return
	}

	if *updateScript == "" {
		return
	}

	log.Infof("Executing %s", *updateScript)
	cmd := exec.Command(*updateScript)

	in, err := cmd.StdinPipe()
	if err != nil {
		log.Error(err)
		return
	}

	outr, outw := io.Pipe()
	if err != nil {
		log.Error(err)
		return
	}
	defer outw.Close()

	cmd.Stdout = outw
	cmd.Stderr = outw

	go func() {
		buf := bufio.NewReader(outr)
		for {
			l, err := buf.ReadString('\n')
			if err != nil {
				if errors.Is(err, io.EOF) {
					log.Infof("OUT> %s", l)
				} else {
					log.Error(err)
				}
				return
			}
			log.Infof("OUT> %s", l)
		}
	}()

	err = cmd.Start()
	if err != nil {
		log.Error(err)
		in.Close()
		return
	}

	w := csv.NewWriter(in)
	for _, domain := range data.Domains {
		err = w.Write([]string{domain.Name, domain.IPv4, domain.IPv6, domain.UpdatedOn.String()})
		if err != nil {
			log.Error(err)
			break
		}
	}
	w.Flush()

	err = in.Close()
	if err != nil {
		log.Error(err)
	}

	err = cmd.Wait()
	if err != nil {
		log.Error(err)
	}
}
