// +build !skip_install

package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
	"gitlab.com/firelizzard/ip-update/files"
	"gitlab.com/firelizzard/ip-update/flags"
)

var appInstall = cobra.Command{
	Use:   "install [*<server>]",
	Short: "Install files",
}

var install = struct {
	service, timer              *bool
	unit, location, description *string
}{
	service:     appInstall.Flags().Bool("service", false, "Install send-ip-update.service"),
	timer:       appInstall.Flags().Bool("timer", false, "Install send-ip-update.timer"),
	unit:        appInstall.Flags().String("unit", "send-ip-update", "Name for unit files"),
	location:    appInstall.Flags().String("location", "/etc/systemd/system", "Install location for unit files"),
	description: appInstall.Flags().String("description", "Send IP Update", "Service description"),
}

func init() {
	appInstall.RunE = runInstall
	app.AddCommand(&appInstall)

	appInstall.MarkFlagDirname("dir")
}

func runInstall(cmd *cobra.Command, args []string) error {
	if !*install.service && !*install.timer {
		fmt.Fprintf(os.Stderr, "Nothing to do; please specify --service and/or --timer\n")
		return nil
	}

	if *install.service && len(args) == 0 {
		return fmt.Errorf("At least one arg is required for --service")
	}

	create := func(ext string) *os.File {
		name := *install.unit + ext
		if *install.location == "-" {
			fmt.Fprintf(os.Stderr, "--- %s ---\n", name)
			return os.Stdout
		}

		name = filepath.Join(*install.location, name)

		f, err := os.Create(name)
		if err != nil {
			log.Printf("create %q: %v", name, err)
			os.Exit(1)
		}
		return f
	}

	tmpl, err := files.LoadClientTemplates()
	if err != nil {
		log.Printf("Failed to load templates: %v", err)
		return nil
	}

	if *install.timer {
		f := create(".timer")
		defer f.Close()

		tmpl.ExecuteTemplate(f, "send-ip-update.timer", struct {
			Description string
		}{*install.description})
	}

	if *install.service {
		f := create(".service")
		defer f.Close()

		flags := flags.Dump(cmd.Flags(), func(f *pflag.Flag) bool {
			return cmd.Parent().Flag(f.Name) == f
		})

		tmpl.ExecuteTemplate(f, "send-ip-update.service", struct {
			Description, BinPath string
			Args                 string
		}{
			Description: *install.description,
			BinPath:     os.Args[0],
			Args:        strings.Join(append(flags, args...), " "),
		})
	}

	return nil
}
