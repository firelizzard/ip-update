package main

import (
	"bufio"
	"crypto"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/spf13/cobra"
	"gitlab.com/firelizzard/ip-update/flags"
	"gitlab.com/firelizzard/ip-update/keys"
)

var Version = "version unknown"

func main() { app.Execute() }

var app = cobra.Command{
	Use:   "send-ip-update <server> [*<server>]",
	Short: "Send an IP update to a depot",
	Args: func(cmd *cobra.Command, args []string) error {
		if cmd.Flag("version").Value.String() == "true" {
			return nil
		}

		if err := cobra.MinimumNArgs(1)(cmd, args); err != nil {
			return err
		}

		if args[0] == "install" {
			fmt.Fprintf(os.Stderr, "compiled without the 'install' command\n")
			os.Exit(1)
		}

		var bad []string
		for _, v := range args {
			u, err := url.Parse(v)
			if err != nil || (u.Scheme != "http" && u.Scheme != "https") {
				bad = append(bad, v)
			}
		}

		if len(bad) > 0 {
			return fmt.Errorf("invalid url(s): %v", strings.Join(bad, ","))
		}
		return nil
	},
}

var version = app.Flags().Bool("version", false, "Show version information")

var key = app.PersistentFlags().StringP("identity", "i", "", "Private key to sign the message with")
var silent = app.PersistentFlags().BoolP("silent", "s", false, "Suppress all output")
var verbose = app.PersistentFlags().BoolP("verbose", "v", false, "Print the server's response")
var ipFrom = flags.UrlArray(app.PersistentFlags(), "ip-from", nil, "Endpoint to query for IP address")
var ipv4 = app.PersistentFlags().IPP("ipv4", "4", nil, "IPv4 address")
var ipv6 = app.PersistentFlags().IPP("ipv6", "6", nil, "IPv6 address")
var continueFrom = app.PersistentFlags().String("continue", "", "Continue (=y) or exit (=n) if --ip-from fails, instead of prompting")

var ipFromOk = true

func init() {
	app.PreRun = preRun
	app.RunE = run

	app.MarkFlagFilename("key")
}

func preRun(cmd *cobra.Command, args []string) {
	for _, ipFrom := range *ipFrom {
		resp, err := http.Get(ipFrom.String())
		if err != nil {
			log.Printf("GET %s: %v", ipFrom, err)
			ipFromOk = false
			continue
		}
		if resp.StatusCode != 200 {
			resp.Body.Close()
			log.Printf("GET %s: %s", ipFrom, resp.Status)
			ipFromOk = false
			continue
		}

		b, err := ioutil.ReadAll(resp.Body)
		resp.Body.Close()
		if err != nil {
			log.Print(err)
			ipFromOk = false
			continue
		}

		ip := net.ParseIP(strings.TrimSpace(string(b)))
		if ip == nil {
			log.Printf("GET %s: response body is not an IP address: %s", ipFrom, b)
			ipFromOk = false
			continue
		}

		if p4 := ip.To4(); len(p4) == net.IPv4len {
			if *ipv4 != nil && !ipv4.Equal(p4) {
				log.Printf("GET %s: overwriting --ipv4 %v with %v", ipFrom, *ipv4, p4)
			} else {
				log.Printf("GET %s: IPv4=%v", ipFrom, p4)
			}
			*ipv4 = p4
		} else if len(ip) == net.IPv6len {
			if *ipv6 != nil && !ipv6.Equal(ip) {
				log.Printf("GET %s: overwriting --ipv6 %v with %v", ipFrom, *ipv6, ip)
			} else {
				log.Printf("GET %s: IPv6=%v", ipFrom, ip)
			}
			*ipv6 = ip
		} else {
			log.Printf("GET %s: invalid IP address: %v", ipFrom, ip)
			ipFromOk = false
			continue
		}
	}
}

func shouldContinueFrom() bool {
	switch strings.ToLower(*continueFrom) {
	default:
		fmt.Printf("Invalid: --continue=%q", *continueFrom)
		fallthrough

	case "":
		fmt.Printf("IPv4=%v IPv6=%v\nContinue? [Y/n]", *ipv4, *ipv6)
		r, err := bufio.NewReader(os.Stdin).ReadString('\n')
		if err != nil {
			if errors.Is(err, io.EOF) {
				return false
			}
			log.Print(err)
			return false
		}

		switch strings.ToLower(r) {
		case "y", "yes":
			return true
		default:
			return false
		}

	case "y", "yes", "1", "true":
		return true

	case "n", "no", "0", "false":
		return false
	}
}

func run(cmd *cobra.Command, args []string) error {
	if *version {
		fmt.Printf("%s %s\n", app.Name(), Version)
		return nil
	}

	if len(*ipFrom) == 0 && *ipv4 == nil && *ipv6 == nil {
		return errors.New("At least one of --ip-from, --ipv4, or --ipv6 must be specified")
	}

	if !ipFromOk && !shouldContinueFrom() {
		return nil
	}

	var signer crypto.Signer
	var method jwt.SigningMethod
	var err error
	var ok bool
	if *key != "" {
		signer, err = keys.LoadPEMFile(*key)
		if err != nil {
			log.Fatal(err)
		}
		method, ok = keys.GetSigningMethod(signer)
		if !ok {
			fmt.Printf("Unsupported key type %T", signer)
			os.Exit(1)
		}

	} else {
		home, err := os.UserHomeDir()
		if err != nil {
			log.Fatal(err)
		}

		ids, err := filepath.Glob(filepath.Join(home, ".ssh", "id_*"))
		if err != nil {
			log.Fatal(err)
		}

		for _, id := range ids {
			signer, err = keys.LoadPEMFile(id)
			if errors.Is(err, keys.ErrUnsupported) {
				continue
			} else if err != nil {
				log.Fatal(err)
			}

			method, ok = keys.GetSigningMethod(signer)
			if ok {
				break
			}
		}

		if !ok {
			fmt.Printf("Could not find any suitable key (~/.ssh/id_*)")
			os.Exit(1)
		}
	}

	fp, err := keys.Fingerprint(signer.Public())
	if err != nil {
		log.Fatal(err)
	}

	tok := jwt.NewWithClaims(method, jwt.StandardClaims{
		IssuedAt: time.Now().Unix(),
		Issuer:   fp,
	})

	tok.Header["ipv4"] = ipv4.String()
	tok.Header["ipv6"] = ipv6.String()

	enc, err := tok.SignedString(signer)
	if err != nil {
		log.Fatal(err)
	}

	var failed bool
	for _, host := range args {
		req, err := http.NewRequest(http.MethodPut, host, strings.NewReader(enc))
		if err != nil {
			log.Print(err)
			continue
		}

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			log.Print(err)
			continue
		}

		if !*silent {
			fmt.Println(resp.Status)
			fmt.Println()

			if *verbose {
				_, err = io.Copy(os.Stdout, resp.Body)
				if err != nil {
					log.Print(err)
				}
			}
		}

		resp.Body.Close()

		if resp.StatusCode >= 400 {
			failed = true
		}
	}

	if failed {
		os.Exit(1)
	}

	return nil
}
