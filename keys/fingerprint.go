package keys

import (
	"crypto"

	"golang.org/x/crypto/ssh"
)

func Fingerprint(key crypto.PublicKey) (string, error) {
	sshKey, err := ssh.NewPublicKey(key)
	if err != nil {
		return "", err
	}

	return ssh.FingerprintSHA256(sshKey), nil
}
