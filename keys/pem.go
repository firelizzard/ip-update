package keys

import (
	"crypto"
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/rsa"
	"errors"
	"fmt"
	"io/ioutil"

	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/ssh"
	"golang.org/x/term"
)

var ErrUnsupported = errors.New("unsupported key type")
var ErrEncrypted = errors.New("key is encrypted")

func loadRawPEMFile(path string) (interface{}, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	s, err := loadRawPEM(data)
	if err == nil {
		return s, nil
	} else if err != ErrEncrypted {
		return nil, err
	}

	fmt.Printf("%q is encrypted\nPassword: ", path)
	pass, err := term.ReadPassword(0)
	if err != nil {
		return nil, err
	}
	return ssh.ParseRawPrivateKeyWithPassphrase(data, pass)
}

func loadRawPEM(data []byte) (interface{}, error) {
	s, err := ssh.ParseRawPrivateKey(data)
	if err == nil {
		return s, nil
	} else if _, ok := err.(*ssh.PassphraseMissingError); ok {
		return nil, ErrEncrypted
	}
	return nil, err
}

func LoadPEMFile(path string) (crypto.Signer, error) {
	raw, err := loadRawPEMFile(path)
	if err != nil {
		return nil, err
	}

	if s, ok := raw.(crypto.Signer); ok {
		return s, nil
	}
	return nil, fmt.Errorf("%w: %T", err, raw)
}

func LoadPEM(data []byte) (crypto.Signer, error) {
	raw, err := loadRawPEM(data)
	if err != nil {
		return nil, err
	}

	if s, ok := raw.(crypto.Signer); ok {
		return s, nil
	}
	return nil, fmt.Errorf("%w: %T", err, raw)
}

func GetSigningMethod(key crypto.Signer) (jwt.SigningMethod, bool) {
	switch key := key.(type) {
	case *ecdsa.PrivateKey:
		switch key.Curve.Params().BitSize {
		case 256:
			return jwt.SigningMethodES256, true
		case 384:
			return jwt.SigningMethodES384, true
		case 521:
			return jwt.SigningMethodES512, true
		}
	case *ed25519.PrivateKey:
		return SigningMethodED256, true
	case *rsa.PrivateKey:
		return jwt.SigningMethodRS256, true
	}

	return nil, false
}
