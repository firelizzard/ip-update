package keys

import (
	"crypto"
	"crypto/ed25519"
	"errors"

	"github.com/dgrijalva/jwt-go"
)

// Specific instances for EC256 and company
var (
	SigningMethodED256 = &SigningMethodED25519{"ED256", crypto.SHA256}
	SigningMethodED384 = &SigningMethodED25519{"ED384", crypto.SHA384}
	SigningMethodED512 = &SigningMethodED25519{"ED512", crypto.SHA512}
)

func init() {
	jwt.RegisterSigningMethod(SigningMethodED256.Alg(), func() jwt.SigningMethod { return SigningMethodED256 })
	jwt.RegisterSigningMethod(SigningMethodED384.Alg(), func() jwt.SigningMethod { return SigningMethodED384 })
	jwt.RegisterSigningMethod(SigningMethodED512.Alg(), func() jwt.SigningMethod { return SigningMethodED512 })
}

type SigningMethodED25519 struct {
	Name string
	Hash crypto.Hash
}

func (m *SigningMethodED25519) key(signingString string, key interface{}) (*ed25519.PrivateKey, error) {
	var privKey *ed25519.PrivateKey
	var ok bool

	if privKey, ok = key.(*ed25519.PrivateKey); !ok {
		return nil, jwt.ErrInvalidKeyType
	}

	return privKey, nil
}

func (m *SigningMethodED25519) Alg() string {
	return m.Name
}

func (m *SigningMethodED25519) Verify(signingString, signature string, key interface{}) error { // Decode the signature
	var sig []byte
	var err error
	if sig, err = jwt.DecodeSegment(signature); err != nil {
		return err
	}

	privKey, err := m.key(signingString, key)
	if err != nil {
		return err
	}

	if ed25519.Verify(privKey.Public().(ed25519.PublicKey), []byte(signingString), sig) {
		return nil
	}
	return errors.New("invalid signature")
}

func (m *SigningMethodED25519) Sign(signingString string, key interface{}) (string, error) {
	privKey, err := m.key(signingString, key)
	if err != nil {
		return "", err
	}

	sig := ed25519.Sign(*privKey, []byte(signingString))
	return jwt.EncodeSegment(sig), nil
}
