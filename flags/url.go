package flags

import (
	"fmt"
	"net/url"
	"strings"

	"github.com/spf13/pflag"
)

func UrlVar(f *pflag.FlagSet, ptr **url.URL, name string, value *url.URL, usage string) {
	f.VarP(newUrlValue(value, ptr), name, "", usage)
}

func UrlVarP(f *pflag.FlagSet, ptr **url.URL, name, shorthand string, value *url.URL, usage string) {
	f.VarP(newUrlValue(value, ptr), name, shorthand, usage)
}

func Url(f *pflag.FlagSet, name string, value *url.URL, usage string) **url.URL {
	ptr := new(*url.URL)
	UrlVarP(f, ptr, name, "", value, usage)
	return ptr
}

func UrlP(f *pflag.FlagSet, name, shorthand string, value *url.URL, usage string) **url.URL {
	ptr := new(*url.URL)
	UrlVarP(f, ptr, name, shorthand, value, usage)
	return ptr
}

func UrlArrayVar(f *pflag.FlagSet, ptr *[]*url.URL, name string, value []*url.URL, usage string) {
	f.VarP(newUrlArrayValue(value, ptr), name, "", usage)
}

func UrlArrayVarP(f *pflag.FlagSet, ptr *[]*url.URL, name, shorthand string, value []*url.URL, usage string) {
	f.VarP(newUrlArrayValue(value, ptr), name, shorthand, usage)
}

func UrlArray(f *pflag.FlagSet, name string, value []*url.URL, usage string) *[]*url.URL {
	ptr := new([]*url.URL)
	UrlArrayVarP(f, ptr, name, "", value, usage)
	return ptr
}

func UrlArrayP(f *pflag.FlagSet, name, shorthand string, value []*url.URL, usage string) *[]*url.URL {
	ptr := new([]*url.URL)
	UrlArrayVarP(f, ptr, name, shorthand, value, usage)
	return ptr
}

func parseUrl(s string) (*url.URL, error) {
	u, err := url.Parse(s)
	if err != nil {
		return nil, fmt.Errorf("cannot set flag: %w", err)
	}
	return u, nil
}

type urlValue struct {
	value **url.URL
}

func newUrlValue(val *url.URL, ptr **url.URL) *urlValue {
	v := new(urlValue)
	v.value = ptr
	*v.value = val
	return v
}

func (v *urlValue) Type() string   { return "string" }
func (v *urlValue) String() string { return (*v.value).String() }

func (v *urlValue) Set(val string) error {
	u, err := parseUrl(val)
	if err != nil {
		return err
	}

	*v.value = u
	return nil
}

type urlArrayValue struct {
	value   *[]*url.URL
	changed bool
}

func newUrlArrayValue(val []*url.URL, ptr *[]*url.URL) *urlArrayValue {
	v := new(urlArrayValue)
	v.value = ptr
	*v.value = val
	return v
}

func (v *urlArrayValue) Type() string { return "urlArray" }

func (v *urlArrayValue) Set(val string) error {
	u, err := parseUrl(val)
	if err != nil {
		return err
	}

	if v.changed {
		*v.value = append(*v.value, u)
	} else {
		*v.value = []*url.URL{u}
		v.changed = true
	}
	return nil
}

func (v *urlArrayValue) Append(val string) error {
	u, err := parseUrl(val)
	if err != nil {
		return err
	}

	*v.value = append(*v.value, u)
	return nil
}

func (v *urlArrayValue) Replace(val []string) error {
	out := make([]*url.URL, len(val))
	for i, val := range val {
		u, err := parseUrl(val)
		if err != nil {
			return err
		}
		out[i] = u
	}

	*v.value = out
	return nil
}

func (v *urlArrayValue) GetSlice() []string {
	out := make([]string, len(*v.value))
	for i, u := range *v.value {
		out[i] = u.String()
	}
	return out
}

func (v *urlArrayValue) String() string {
	var b strings.Builder
	b.WriteRune('[')
	for i, u := range *v.value {
		if i != 0 {
			b.WriteRune(',')
		}
		b.WriteRune('"')
		b.WriteString(u.String())
		b.WriteRune('"')
	}
	b.WriteRune(']')
	return b.String()
}
