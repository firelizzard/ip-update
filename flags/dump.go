package flags

import "github.com/spf13/pflag"

func Dump(fs *pflag.FlagSet, predicate func(f *pflag.Flag) bool) []string {
	if predicate == nil {
		predicate = func(f *pflag.Flag) bool { return true }
	}
	s := []string{}
	fs.Visit(func(f *pflag.Flag) {
		if !predicate(f) {
			return
		}

		switch v := f.Value.(type) {
		case pflag.SliceValue:
			for _, v := range v.GetSlice() {
				s = append(s, "--"+f.Name, v)
			}
		default:
			s = append(s, "--"+f.Name, v.String())
		}
	})
	return s
}
